package com.example.gebrdenbersetzer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;

import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    ImageView ivMic;
    TextView tvSpeech;
    GifImageView gif;
    public static final int REQUEST_CODE_SPEECH_INPUT = 1;

    final String howAreYou = "how are you";
    final String canIBorrowThat = "can i borrow that";
    final String april = "april";
    final String blizzard = "blizzard";
    final String cloudy = "cloudy";
    final String cold = "cold";
    final String cool = "cool";
    final String december = "december";
    final String dontUnderstand = "i don't understand";
    final String february = "february";
    final String goodEvening = "good evening";
    final String goodAfternoon = "good afternoon";
    final String goodMorning = "good morning";
    final String goodNight = "goodnight";
    final String hello = "hello";
    final String hot = "hot";
    final String howAreYouDoing = "how are you doing";
    final String iKnow = "i know";
    final String isItColdinHere = "is it cold in here";
    final String iUnderstand = "i understand";
    final String january = "january";
    final String july = "july";
    final String march = "march";
    final String maybe = "maybe";
    final String november = "november";
    final String repeat = "repeat";
    final String snow = "snow";
    final String spring = "spring";
    final String summer = "summer";
    final String whatsForLunch = "what's for lunch";
    final String whichBuildung = "which building";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivMic = findViewById(R.id.ivMic);
        tvSpeech = findViewById(R.id.tvSpeech);
        gif = findViewById(R.id.gif);

        ivMic.setOnClickListener(v -> {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");

            try {
                startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT);
            } catch (Exception e) {
                Toast.makeText(this, "Exeption" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SPEECH_INPUT) {
            if (resultCode == RESULT_OK && data != null) {

                ArrayList<String> result = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String voiceInput = Objects.requireNonNull(result).get(0).toLowerCase();

                if (voiceInput.contains(howAreYou)) {
                    tvSpeech.setText("Du sagtest: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.how_are_you_doing);
                } else if (voiceInput.contains(howAreYouDoing)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.how_are_you_doing);
                } else if (voiceInput.contains(april)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.april);
                } else if (voiceInput.contains(blizzard)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.blizzard);
                } else if (voiceInput.contains(cloudy)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.cloudy);
                } else if (voiceInput.contains(cold)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.cold);
                } else if (voiceInput.contains(cool)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.cool);
                } else if (voiceInput.contains(december)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.december);
                } else if (voiceInput.contains(dontUnderstand)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.dontunderstand);
                } else if (voiceInput.contains(february)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.february);
                } else if (voiceInput.contains(goodEvening)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.goodevening);
                } else if (voiceInput.contains(goodAfternoon)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.goodfternoon);
                } else if (voiceInput.contains(goodMorning)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.goodmorning);
                } else if (voiceInput.contains(goodNight)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.goodnight);
                } else if (voiceInput.contains(hello)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.hello);
                } else if (voiceInput.contains(hot)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.hot);
                } else if (voiceInput.contains(iKnow)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.iknow);
                } else if (voiceInput.contains(isItColdinHere)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.isitcoldinhere);
                } else if (voiceInput.contains(iUnderstand)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.iunderstand);
                } else if (voiceInput.contains(january)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.january);
                } else if (voiceInput.contains(july)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.july);
                } else if (voiceInput.contains(march)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.march);
                } else if (voiceInput.contains(maybe)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.maybe);
                } else if (voiceInput.contains(november)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.november);
                } else if (voiceInput.contains(repeat)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.repeat);
                } else if (voiceInput.contains(snow)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.snow);
                } else if (voiceInput.contains(spring)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.spring);
                } else if (voiceInput.contains(summer)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.summer);
                } else if (voiceInput.contains(whatsForLunch)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.whatsforlunch);
                } else if (voiceInput.contains(whichBuildung)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.whichbuilding);
                } else if (voiceInput.contains(canIBorrowThat)) {
                    tvSpeech.setText("You said: " + Objects.requireNonNull(result).get(0));
                    gif.setImageResource(R.drawable.caniborrowthat);
                } else {
                    tvSpeech.setText("Please repeat");
                    gif.setImageResource(R.drawable.repeat);
                }
            }


        }
    }
}
